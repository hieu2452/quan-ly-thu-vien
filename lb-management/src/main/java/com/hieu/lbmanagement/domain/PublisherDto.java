package com.hieu.lbmanagement.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PublisherDto {
    private long id;
    private String publisherName;
}
