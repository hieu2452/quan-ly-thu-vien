package com.hieu.lbmanagement.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AuthorDto {
    private long id;
    private String authorName;
}
