package com.hieu.lbmanagement.repository;

import com.hieu.lbmanagement.entity.Author;
import com.hieu.lbmanagement.entity.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PublisherRepository extends JpaRepository<Publisher,Long>{
}

