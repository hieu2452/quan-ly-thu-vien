package com.hieu.lbmanagement.repository;

import com.hieu.lbmanagement.entity.RefreshToken;
import com.hieu.lbmanagement.entity.Staff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

import java.util.Optional;

public interface RefreshTokenRepository extends JpaRepository<RefreshToken,Long> {
    Optional<RefreshToken> findByToken(String token);
    Optional<RefreshToken> findByStaff(Staff staff);
    @Modifying
    int deleteByStaff(Staff staff);
}
