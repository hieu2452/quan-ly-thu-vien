package com.hieu.lbmanagement.exception;

public class TokenExpiredException extends RuntimeException{
    public TokenExpiredException(String message) {        super(message);
    }
}
