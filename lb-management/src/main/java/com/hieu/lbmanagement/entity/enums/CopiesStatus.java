package com.hieu.lbmanagement.entity.enums;

public enum CopiesStatus {
    AVAILABLE,
    RESERVED,
    CHECKED_OUT
}
