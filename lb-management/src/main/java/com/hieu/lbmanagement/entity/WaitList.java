package com.hieu.lbmanagement.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.print.Doc;

@Entity
@Data
@Builder
@AllArgsConstructor
public class WaitList {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @ManyToOne
    private Document document;
    @ManyToOne
    private Student student;
    public WaitList() {

    }
}
