package com.hieu.lbmanagement.entity.enums;

public enum FineStatus {
    PAID,
    UNPAID
}
