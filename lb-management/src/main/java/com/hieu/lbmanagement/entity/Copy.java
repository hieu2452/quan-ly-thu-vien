package com.hieu.lbmanagement.entity;

import com.hieu.lbmanagement.entity.enums.CopiesStatus;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.SuperBuilder;

@Entity
@AllArgsConstructor
@Data
@Builder
public class Copy {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long copyId;

    private CopiesStatus status;

    @ManyToOne
    @JoinColumn(name = "document_id")
    private Document document;

    public Copy() {

    }
}
