package com.hieu.lbmanagement.entity;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import javax.print.Doc;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@AllArgsConstructor
@Data
@SuperBuilder
@DiscriminatorValue(value = "article")
public class Article extends Document {
    private Date issuedDate;
    private int issuedNumber;
    @ManyToOne
    @JoinColumn(name = "publisher_id")
    public Publisher publisher;

//    public Article(long documentId, String title, String description, List<Copy> copies, List<Author> author, String language, String identifier, Date issuedDate, int issuedNumber, Publisher publisher) {
//        super(documentId, title, description,copies, author, language, identifier);
//        this.issuedDate = issuedDate;
//        this.issuedNumber = issuedNumber;
//        this.publisher = publisher;
//    }

    public Article () {

    }
}
