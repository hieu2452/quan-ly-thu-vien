package com.hieu.lbmanagement.entity;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import javax.print.Doc;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@AllArgsConstructor
@Data
@SuperBuilder
@DiscriminatorValue(value = "research")
public class Research extends Document {
    private Date defenseDate;
    @OneToOne
    @JoinColumn(name = "department_id")
    private Department department;
    public Research () {

    }
//    public Research(long documentId, String title, String description, List<Copy> copies, List<Author> author, String language, String identifier, Date defenseDate, Department department) {
//        super(documentId, title, description,copies, author, language, identifier);
//        this.defenseDate = defenseDate;
//        this.department = department;
//    }
}
