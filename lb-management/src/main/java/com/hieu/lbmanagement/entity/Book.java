package com.hieu.lbmanagement.entity;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@AllArgsConstructor
@Data
@SuperBuilder
@DiscriminatorValue(value = "book")
public class Book extends Document{

    private int publishYear;

    @ManyToOne
    @JoinColumn(name = "publisher_id")
    public Publisher publisher;

    public Book() {}

//    public Book(long documentId, String title, List<Copy> copies , String description,List<Author> author, String language, String identifier, int publishYear, Publisher publisher) {
//        super(documentId, title, description, copies, author, language, identifier);
//        this.publishYear = publishYear;
//        this.publisher = publisher;
//    }
}
