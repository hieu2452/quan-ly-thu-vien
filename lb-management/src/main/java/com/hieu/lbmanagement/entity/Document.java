package com.hieu.lbmanagement.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Entity
@AllArgsConstructor
@Data
@SuperBuilder
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="document_type",
        discriminatorType = DiscriminatorType.STRING)
public class Document {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long documentId;

    private String title;

    private String description;

    @OneToMany(mappedBy = "document")
    private List<Copy> copies;

//    @OneToOne(fetch = FetchType.EAGER)
//    private Research research;
//    @OneToOne(fetch = FetchType.EAGER)
//    private Book book;
//    @OneToOne(fetch = FetchType.EAGER)
//    private Article article;

    @ManyToMany
    @JoinTable(
            name = "document_author",
            joinColumns = @JoinColumn(name = "document_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id"))
    private List<Author> authors;

    private String language;

    private String identifier;

    public Document() {

    }

}
