package com.hieu.lbmanagement.entity;

import com.hieu.lbmanagement.entity.enums.FineStatus;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Entity
@Data
@Builder
@AllArgsConstructor
public class Fine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private Date fineDate;
    private double fineAmount;
    private FineStatus status;
    @ManyToOne
    private Checkout checkout;
    @ManyToOne
    private Student student;

}
