package com.hieu.lbmanagement;

import com.hieu.lbmanagement.entity.Role;
import com.hieu.lbmanagement.entity.Staff;
import com.hieu.lbmanagement.repository.RoleRepository;
import com.hieu.lbmanagement.repository.StaffRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collection;
import java.util.Collections;

@SpringBootApplication
public class LbManagementApplication implements CommandLineRunner {
	@Autowired
	private StaffRepository staffRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private PasswordEncoder passwordEncoder;
	public static void main(String[] args) {
		SpringApplication.run(LbManagementApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Staff staff = new Staff();
		Role role = roleRepository.findByRoleName("ADMIN");

		staff.setId(201210121);
		staff.setAge(30);
		staff.setPassword(passwordEncoder.encode("12345"));
		staff.setFirstName("Hieu");
		staff.setRole(Collections.singletonList(role));

		staffRepository.save(staff);
	}
}
