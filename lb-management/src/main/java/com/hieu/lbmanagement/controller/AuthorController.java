package com.hieu.lbmanagement.controller;

import com.hieu.lbmanagement.domain.AuthorDto;
import com.hieu.lbmanagement.entity.Author;
import com.hieu.lbmanagement.service.IAuthorService;
import com.hieu.lbmanagement.service.impl.AuthorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/author")
public class AuthorController {
    private final IAuthorService authorService;

    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping
    public ResponseEntity<List<AuthorDto>> getAllAuthors() {
        List<AuthorDto> authors = authorService.getAllAuthors();
        return new ResponseEntity<>(authors, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AuthorDto> getAuthorById(@PathVariable("id") long id) {
        AuthorDto author = authorService.getAuthorById(id);
        return author != null ?
                new ResponseEntity<>(author, HttpStatus.OK) :
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/")
    public ResponseEntity<AuthorDto> save(@RequestBody AuthorDto authorDto) {
        return new ResponseEntity<>(authorService.save(authorDto), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<AuthorDto> updateAuthor(@PathVariable("id") long id , @RequestBody AuthorDto authorDto){
        AuthorDto updateAuthor = authorService.update(id, authorDto);
        return updateAuthor != null ?
                new ResponseEntity<>(updateAuthor, HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable("id") long id){
        authorService.delete(id);
        return  new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
