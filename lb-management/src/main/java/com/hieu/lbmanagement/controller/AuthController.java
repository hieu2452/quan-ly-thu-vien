package com.hieu.lbmanagement.controller;

import com.hieu.lbmanagement.domain.request.LoginRequest;
import com.hieu.lbmanagement.domain.response.AuthResponse;
import com.hieu.lbmanagement.domain.response.RefreshTokenResponse;
import com.hieu.lbmanagement.service.IAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private IAuthenticationService authenticationService;

    @PostMapping("/login")
    public ResponseEntity<AuthResponse> login(@RequestBody LoginRequest loginRequest) {
        return new ResponseEntity<>(authenticationService.login(loginRequest), HttpStatus.OK);
    }

    @GetMapping("refreshtoken/{refreshToken}")
    public ResponseEntity<RefreshTokenResponse> refreshToken(@PathVariable String refreshToken) {

        return new ResponseEntity<>(authenticationService.refreshToken(refreshToken),HttpStatus.OK);
    }
}
