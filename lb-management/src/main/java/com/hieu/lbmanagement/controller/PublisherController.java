package com.hieu.lbmanagement.controller;

import com.hieu.lbmanagement.domain.AuthorDto;
import com.hieu.lbmanagement.domain.PublisherDto;
import com.hieu.lbmanagement.entity.Publisher;
import com.hieu.lbmanagement.service.IPublisherService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/publisher")
public class PublisherController {
    private final IPublisherService publisherService;


    public PublisherController(IPublisherService publisherService) {
        this.publisherService = publisherService;
    }

    @GetMapping
    public ResponseEntity<List<PublisherDto>> getAllPublisher(){
        List<PublisherDto> publishers = publisherService.getAllPublishers();
        return new ResponseEntity<>(publishers, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PublisherDto> getPublisherById(@PathVariable("id") long id) {
        PublisherDto publisher = publisherService.getPublisherById(id);
        return publisher != null ?
                new ResponseEntity<>(publisher, HttpStatus.OK) :
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/")
    public ResponseEntity<PublisherDto> save(@RequestBody PublisherDto publisherDto) {
        return new ResponseEntity<>(publisherService.save(publisherDto), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<PublisherDto> updatePublisher(@PathVariable("id") long id , @RequestBody PublisherDto publisherDto){
        PublisherDto updatePublisher = publisherService.update(id, publisherDto);
        return updatePublisher != null ?
                new ResponseEntity<>(updatePublisher, HttpStatus.OK):
                new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable("id") long id){
        publisherService.delete(id);
        return  new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
