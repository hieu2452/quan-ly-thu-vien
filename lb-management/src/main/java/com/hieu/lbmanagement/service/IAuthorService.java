package com.hieu.lbmanagement.service;

import com.hieu.lbmanagement.domain.AuthorDto;

import java.util.List;

public interface IAuthorService {

    List<AuthorDto> getAllAuthors();
    AuthorDto getAuthorById(long id);
    AuthorDto save(AuthorDto authorDto);
    AuthorDto update(long id, AuthorDto authorDto);
    void delete(long id);
}
