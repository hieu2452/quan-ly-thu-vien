package com.hieu.lbmanagement.service.impl;

import com.hieu.lbmanagement.domain.AuthorDto;
import com.hieu.lbmanagement.domain.PublisherDto;
import com.hieu.lbmanagement.entity.Author;
import com.hieu.lbmanagement.entity.Publisher;
import com.hieu.lbmanagement.helper.AuthorMapper;
import com.hieu.lbmanagement.helper.PublisherMapper;
import com.hieu.lbmanagement.repository.PublisherRepository;
import com.hieu.lbmanagement.service.IPublisherService;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PublisherService implements IPublisherService {
    private final PublisherRepository publisherRepository;

    public PublisherService(PublisherRepository publisherRepository){
        this.publisherRepository = publisherRepository;
    }

    @Transactional
    @Override
    public PublisherDto save(PublisherDto publisherDto){
        Publisher publisher = Publisher.builder()
                .publisherName(publisherDto.getPublisherName())
                .build();
        return PublisherMapper.map(publisherRepository.save(publisher));
    }

    @Transactional
    @Override
    public List<PublisherDto> getAllPublishers() {
        List<Publisher> publishers = publisherRepository.findAll();
        return publishers.stream()
                .map(PublisherMapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public PublisherDto getPublisherById(long id) {
        Optional<Publisher> publisher = publisherRepository.findById(id);
        return publisher.map(PublisherMapper::map).orElse(null);
    }

    @Transactional
    @Override
    public PublisherDto update(long id, PublisherDto publisherDto){
        Optional<Publisher> optionalPublisher = publisherRepository.findById(id);
        if (optionalPublisher.isPresent()){
            Publisher publisher = optionalPublisher.get();
            publisher.setPublisherName(publisher.getPublisherName());
            return PublisherMapper.map(publisherRepository.save(publisher));
        }else {
            throw new EntityNotFoundException("Không tìm thấy tên NXB có id: " +id);
        }
    }

    @Transactional
    @Override
    public void delete(long id){
        Optional<Publisher> optionalPublisher = publisherRepository.findById(id);
        if (optionalPublisher.isPresent()){
            publisherRepository.delete(optionalPublisher.get());
        }else {
            throw new EntityNotFoundException("Không tìm thấy tên NXB có id: " +id);
        }
    }

}
