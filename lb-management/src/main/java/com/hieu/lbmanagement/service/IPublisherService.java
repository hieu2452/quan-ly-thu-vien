package com.hieu.lbmanagement.service;

import com.hieu.lbmanagement.domain.PublisherDto;

import java.util.List;

public interface IPublisherService {
    List<PublisherDto> getAllPublishers();
    PublisherDto getPublisherById(long id);
    PublisherDto save(PublisherDto publisherDto);
    PublisherDto update(long id, PublisherDto publisherDto);
    void delete(long id);
}
