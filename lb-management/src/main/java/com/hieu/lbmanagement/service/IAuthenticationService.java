package com.hieu.lbmanagement.service;

import com.hieu.lbmanagement.domain.request.LoginRequest;
import com.hieu.lbmanagement.domain.response.AuthResponse;
import com.hieu.lbmanagement.domain.response.RefreshTokenResponse;
import com.hieu.lbmanagement.entity.RefreshToken;

public interface IAuthenticationService {
    AuthResponse login(LoginRequest loginRequest);

    RefreshTokenResponse refreshToken(String refreshToken);
}
