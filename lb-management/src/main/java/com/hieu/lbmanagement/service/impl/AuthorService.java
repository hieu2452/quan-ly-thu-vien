package com.hieu.lbmanagement.service.impl;

import com.hieu.lbmanagement.domain.AuthorDto;
import com.hieu.lbmanagement.entity.Author;
import com.hieu.lbmanagement.helper.AuthorMapper;
import com.hieu.lbmanagement.repository.AuthorRepository;
import com.hieu.lbmanagement.service.IAuthorService;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.stream.Collectors;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorService implements IAuthorService {
    private final AuthorRepository authorRepository;

    public AuthorService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Transactional
    @Override
    public List<AuthorDto> getAllAuthors() {
        List<Author> authors = authorRepository.findAll();
        return authors.stream()
                .map(AuthorMapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public AuthorDto getAuthorById(long id) {
        Optional<Author> author = authorRepository.findById(id);
        return author.map(AuthorMapper::map).orElse(null);
    }

    @Transactional
    @Override
    public AuthorDto save(AuthorDto authorDto) {
        Author author = Author.builder()
                .authorName(authorDto.getAuthorName())
                .build();
        return AuthorMapper.map(authorRepository.save(author));
    }

    @Transactional
    @Override
    public AuthorDto update(long id, AuthorDto authorDto){
        Optional<Author> optionalAuthor = authorRepository.findById(id);
        if (optionalAuthor.isPresent()){
            Author author = optionalAuthor.get();
            author.setAuthorName(authorDto.getAuthorName());
            return AuthorMapper.map(authorRepository.save(author));
        }else {
            throw new EntityNotFoundException("Không tìm thấy tên tác giả có id: " +id);
        }
    }

    @Transactional
    @Override
    public void delete(long id){
        Optional<Author> optionalAuthor = authorRepository.findById(id);
        if (optionalAuthor.isPresent()){
            authorRepository.delete(optionalAuthor.get());
        }else {
            throw new EntityNotFoundException("Không tìm thấy tên tác giả có id: " +id);
        }
    }


}
