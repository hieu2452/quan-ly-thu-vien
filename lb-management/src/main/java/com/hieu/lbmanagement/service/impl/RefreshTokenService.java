package com.hieu.lbmanagement.service.impl;

import com.hieu.lbmanagement.entity.RefreshToken;
import com.hieu.lbmanagement.entity.Staff;
import com.hieu.lbmanagement.exception.TokenRefreshException;
import com.hieu.lbmanagement.repository.RefreshTokenRepository;
import com.hieu.lbmanagement.repository.StaffRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Service
public class RefreshTokenService {
    @Value("${spring.refresh-token.time-to-live}")
    private Long refreshTokenTTL;

    @Autowired
    private RefreshTokenRepository refreshTokenRepository;

    @Autowired
    private StaffRepository staffRepository;

    public Optional<RefreshToken> findByToken(String token) {
        return refreshTokenRepository.findByToken(token);
    }

    public RefreshToken createRefreshToken(Staff staff) {
        RefreshToken refreshToken = new RefreshToken();
        Optional<RefreshToken> optional = refreshTokenRepository.findByStaff(staff);

        if(optional.isPresent()) {
            return optional.get();
        }

        refreshToken.setStaff(staffRepository.findById(staff.getId()).get());
        refreshToken.setExpiryDate(Instant.now().plusMillis(refreshTokenTTL));
        refreshToken.setToken(UUID.randomUUID().toString());

        refreshToken = refreshTokenRepository.save(refreshToken);
        return refreshToken;
    }

    public RefreshToken verifyExpiration(RefreshToken token) {
        if (token.getExpiryDate().compareTo(Instant.now()) < 0) {
            refreshTokenRepository.delete(token);
            throw new TokenRefreshException(token.getToken(), "Refresh token was expired. Please make a new signin request");
        }

        return token;
    }

    @Transactional
    public int deleteByUserId(Long userId) {
        return refreshTokenRepository.deleteByStaff(staffRepository.findById(userId).get());
    }
}
