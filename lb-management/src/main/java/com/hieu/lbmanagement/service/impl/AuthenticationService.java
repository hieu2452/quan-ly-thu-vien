package com.hieu.lbmanagement.service.impl;

import com.hieu.lbmanagement.domain.request.LoginRequest;
import com.hieu.lbmanagement.domain.response.AuthResponse;
import com.hieu.lbmanagement.domain.response.RefreshTokenResponse;
import com.hieu.lbmanagement.entity.RefreshToken;
import com.hieu.lbmanagement.entity.Role;
import com.hieu.lbmanagement.entity.Staff;
import com.hieu.lbmanagement.exception.TokenRefreshException;
import com.hieu.lbmanagement.repository.StaffRepository;
import com.hieu.lbmanagement.security.JwtGenerator;
import com.hieu.lbmanagement.service.IAuthenticationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AuthenticationService implements IAuthenticationService {
    private final AuthenticationManager authenticationManager;
    private final StaffRepository staffRepository;
    private final JwtGenerator jwtGenerator;
    private final RefreshTokenService refreshTokenService;
    public AuthenticationService(AuthenticationManager authenticationManager, StaffRepository staffRepository, JwtGenerator jwtGenerator, RefreshTokenService refreshTokenService) {
        this.authenticationManager = authenticationManager;
        this.staffRepository = staffRepository;
        this.jwtGenerator = jwtGenerator;
        this.refreshTokenService = refreshTokenService;
    }

    @Override
    public AuthResponse login(LoginRequest loginRequest) {
        Authentication authentication;
        try {
            authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            loginRequest.getUsername(),
                            loginRequest.getPassword()
                    )
            );
        }catch (Exception e){
            throw new BadCredentialsException("username or password is incorrect");
        }

        Staff user = (Staff) authentication.getPrincipal();

        Map<String, List<String>> roles = mapRole(user.getRole());
        RefreshToken refreshToken = refreshTokenService.createRefreshToken(user);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        var token = jwtGenerator.generateToken(roles,user);
        return new AuthResponse(loginRequest.getUsername(),token,refreshToken.getToken());
    }

    @Override
    public RefreshTokenResponse refreshToken(String refreshToken) {
        return refreshTokenService.findByToken(refreshToken)
                .map(refreshTokenService::verifyExpiration)
                .map(RefreshToken::getStaff)
                .map(user -> {
                    Map<String,List<String>> roles = mapRole(user.getRole());
                    String token = jwtGenerator.generateToken(roles,user);
                    return new RefreshTokenResponse(token, refreshToken);
                })
                .orElseThrow(() -> new TokenRefreshException(refreshToken,
                        "Refresh token is not in database!"));
    }

    private Map<String, List<String>> mapRole(List<Role> roles){
        List<String> roleName = roles.stream().map(Role::getRoleName).collect(Collectors.toList());
        Map<String,List<String>> authorities = new HashMap<>();
        authorities.put("role",roleName);
        return authorities;
    }
}
