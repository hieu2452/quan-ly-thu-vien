package com.hieu.lbmanagement.helper;

import com.hieu.lbmanagement.domain.PublisherDto;
import com.hieu.lbmanagement.entity.Publisher;

public class PublisherMapper {

    public static PublisherDto map(Publisher publisher){
        return PublisherDto.builder()
                .id(publisher.getId())
                .publisherName(publisher.getPublisherName())
                .build();
    }

    public static Publisher map(PublisherDto publisherDto){
        return Publisher.builder()
                .id(publisherDto.getId())
                .publisherName(publisherDto.getPublisherName())
                .build();
    }
}
