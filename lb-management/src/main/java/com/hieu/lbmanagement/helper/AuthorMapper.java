package com.hieu.lbmanagement.helper;

import com.hieu.lbmanagement.domain.AuthorDto;
import com.hieu.lbmanagement.entity.Author;

public class AuthorMapper {
    public static AuthorDto map(Author author) {
        return AuthorDto.builder()
                .id(author.getId())
                .authorName(author.getAuthorName())
                .build();
    };

    public static Author map(AuthorDto author) {
        return Author.builder()
                .id(author.getId())
                .authorName(author.getAuthorName())
                .build();
    };
}
